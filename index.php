<?php


use Zrx\Qdd\fdd\Fdd;

require 'vendor/autoload.php';

function dd(...$values){
    die(json_encode($values));
}
$url = "https://anshu2022.oss-cn-shanghai.aliyuncs.com/2023-01/09/bb3f8fdf95c7ce0510f55d4781d0798e1b97bb11.PDF";

$arr = [
    'fileType'=>'doc',
    'fileUrl'=>urlencode($url),
];

$fdd = new Fdd();
$token = $fdd->getAccessToken();
$response = $fdd->docClient->fileUploadByUrl($token,$arr);
dd($response);
$fdd->fileUploadByUrl();