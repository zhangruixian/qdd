<?php

namespace Zrx\Qdd\fdd\constants;

/**
 * 基础API参数常量
 */
class OpenApiConfigConstants
{
    /**
     * 接口请求域名
     *
     * 生产环境域名：https://api.fadada.com/api/v5/
     * 开发环境域名：https://uat-api.fadada.com/api/v5/
     *
     */
    const SERVICE_URL = "https://uat-api.fadada.com/api/v5/";

    /**
     * 应用appid
     */
    const APP_ID = "00000862";

    /**
     * 应用秘钥
     */
    const APP_SECRET = "YERBLOWWOYKLGFNQBLUJU7NK0HVBCD6U";

}
