<?php

namespace Zrx\Qdd\fdd\api;

use Zrx\Qdd\fdd\client\IClient;
use Zrx\Qdd\fdd\constants\OpenApiConfigConstants;
use Zrx\Qdd\fdd\constants\OpenApiUrlConstants;

/**
 * EUI页面链接管理
 * 对EUI页面链接进行管理操作，如个人授权、新企业授权、计费页面链接等
 */
class EUIClient
{
    private $client;

    public function __construct(IClient $client)
    {
        $this->client = $client;
    }


    # 获取应用级资源访问链接
    function getAppPageResourceUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken,  json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_PAGE_RESOURCE_GET_URL);
    }

    # 获取用户级资源访问链接
    function getUserPageResourceUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken,  json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::USER_PAGE_RESOURCE_GET_URL);
    }

    private function getEuiDomain($self): string
    {
        $domain = array(
            "https://sit-api.fadada.com/api/v5/"=>"https://".OpenApiConfigConstants::APP_ID.".sit-e.fadada.com/authorize/list?",
            "https://uat-api.fadada.com/api/v5/"=>"https://".OpenApiConfigConstants::APP_ID.".uat-e.fadada.com/authorize/list?",
            "https://api.fadada.com/api/v5/"=>"https://".OpenApiConfigConstants::APP_ID.".e.fadada.com/authorize/list?"
        );
        return $domain[$self] ;
    }

    # 获取计费链接
    function get_bill_url(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::BILLING_GET_BILL_URL);
    }


}