<?php

namespace Zrx\Qdd\fdd\api;

use Zrx\Qdd\fdd\client\IClient;
use Zrx\Qdd\fdd\constants\OpenApiUrlConstants;

/**
 * 签署任务管理
 * 包含签署任务的创建、维护、各个流程节点的流转操作，以及签署任务文件下载
 */
class SignTaskClient
{
    private $client;

    public function __construct(IClient $client)
    {
        $this->client = $client;
    }

    # 创建签署任务
    function create(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_CREATE);
    }

    # 创建签署任务 (基于签署模板)
    function createWithTemplate(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_CREATE_WITH_TEMPLATE);
    }

    # 添加签署任务文档
    function addDocs(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_ADD_DOCS);
    }

    # 移除签署任务文档
    function deleteDocs(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_DELETE_DOCS);
    }

    # 添加签署任务附件
    function addAttachs(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_ADD_ATTACHS);
    }

    # 移除签署任务附件
    function deleteAttachs(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_DELETE_ATTACHS);
    }

    # 添加签署任务控件
    function addFields(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_ADD_FIELD);
    }

    # 移除签署任务控件
    function deleteFields(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_DELETE_FIELD);
    }

    # 添加签署任务参与方
    function addActors(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_ADD_ACTORS);
    }

    # 移除签署任务参与方
    function deleteActors(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_DELETE_ACTOR);
    }

    # 获取签署任务参与方专属链接
    function actorGetUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_ACTOR_GET_URL);
    }

    # 提交签署任务
    function start(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_START);
    }

    # 填写签署任务控件内容
    function fillFieldsValue(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_FILL_FIELDS_VALUE);
    }

    # 定稿签署任务文档
    function finalizeDoc(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_DOC_FINALIZE);
    }

    # 阻塞签署任务
    function block(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_BLOCK);
    }

    # 解阻签署任务
    function unblock(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_UNBLOCK);
    }

    # 撤销签署任务
    function cancel(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_CANCEL);
    }

    # 获取签署任务编辑链接
    function getEditUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_GET_EDIT_URL);
    }

    # 获取签署任务预览链接
    function getPreviewUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_GET_PREVIEW_URL);
    }

    # 获取签署任务批量签署链接
    function getBatchSignUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_GET_BATCH_SIGN_URL);
    }

    # 催办签署任务
    function urge(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_URGE);
    }

    # 查询企业签署任务文件夹
    function getCatalogList(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::SIGN_TASK_CATALOG_LIST);
    }

    # 获取应用的签署任务详情
    function getAppDetail(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::SIGN_TASK_APP_GET_DETAIL);
    }

    # 查询签署任务控件信息
    function getFieldList(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::SIGN_TASK_FIELD_GET_LIST);
    }

    # 查询签署任务参与方信息
    function getActorList(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::SIGN_TASK_ACTOR_GET_LIST);
    }

    # 查询签署任务审批信息
    function getApprovalInfo(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::SIGN_TASK_APPROVAL_GET_INFO);
    }

    # 获取指定归属方的签署任务文档下载地址
    function getOwnerDownloadUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::SIGN_TASK_OWNER_GET_DOWNLOAD_URL);
    }

    # 获取指定归属方的签署任务列表
    function getOwnerList(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SIGN_TASK_OWNER_GET_LIST);
    }
}
