<?php

namespace Zrx\Qdd\fdd\api;

use Zrx\Qdd\fdd\client\IClient;
use Zrx\Qdd\fdd\constants\OpenApiUrlConstants;

/**
 * 印章管理
 * 包含印章列表和用企业印员列表
 */
class SealClient
{
    private $client;

    public function __construct(IClient $client)
    {
        $this->client = $client;
    }

    # 获取印章管理链接
    function getSealManageUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_MANAGE_GET_URL);
    }

    # 获取设置企业印章免验证签链接
    function getSealFreeSignUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_FREE_SIGN_GET_URL);
    }

    # 获取印章创建链接
    function getSealCreateUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_CREATE_GET_URL);
    }

    # 查询印章列表
    function getSealList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_GET_LIST);
    }

    # 查询印章详情
    function getSealDetail(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_GET_DETAIL);
    }

    # 获取指定印章详情链接
    function getAppointedSealUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_MANAGE_GET_APPOINTED_SEAL_URL);
    }

    # 查询企业用印员列表
    function getSealUserList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_GET_USER_LIST);
    }

    # 查询指定成员的印章列表
    function getAppointedUserSealList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_USER_GET_LIST);
    }

    # 查询审核中的印章列表
    function getVerifySealList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_VERIFY_GET_LIST);
    }

    # 修改印章基本信息
    function sealModify(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_MODIFY);
    }

    # 获取设置用印员链接
    function getSealGrantUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_GRANT_GET_URL);
    }

    # 解除印章授权
    function sealGrantCancel(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_GRANT_CANCEL);
    }

    # 设置印章状态
    function sealSetStatus(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_SET_STATUS);
    }

    # 删除印章
    function sealDelete(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::SEAL_DELETE);
    }

    # 查询个人签名列表
    function getPersonalSealList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::PERSONAL_SEAL_GET_LIST);
    }

    # 获取设置个人签名免验证签链接
    function getPersonalSealFreeSignUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::PERSONAL_SEAL_FREE_SIGN_GET_URL);
    }

}
