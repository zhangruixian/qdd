<?php

namespace Zrx\Qdd\fdd\api;

use Zrx\Qdd\fdd\client\IClient;
use Zrx\Qdd\fdd\constants\OpenApiUrlConstants;

/**
 * 文档模板管理
 * 包含文档模板列表、文档模板详情查询、签署模板列表、签署模板详情查询
 */
class TemplateClient
{
    private $client;

    public function __construct(IClient $client)
    {
        $this->client = $client;
    }

    # 查询文档模板列表
    function getDocTemplateList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::DOC_TEMPLATE_GET_LIST);
    }

    # 获取文档模板详情
    function getDocTemplateDetail(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::DOC_TEMPLATE_GET_DETAIL);
    }

    # 查询签署模板列表
    function getSignTemplateList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::SIGN_TEMPLATE_GET_LIST);
    }

    # 获取签署模板详情
    function getSignTemplateDetail(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::SIGN_TEMPLATE_GET_DETAIL);
    }

    # 获取模板新增链接
    function templateCreateGetUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::TEMPLATE_CREATE_GET_URL);
    }

    # 获取模板编辑链接
    function templateEditGetUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::TEMPLATE_EDIT_GET_URL);
    }

    # 获取模板预览链接
    function templatePreviewGetUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::TEMPLATE_PREVIEW_GET_URL);
    }

    # 获取模板管理链接
    function templateManageGetUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::TEMPLATE_MANAGE_GET_URL);
    }
}
