<?php

namespace Zrx\Qdd\fdd\api;

use Zrx\Qdd\fdd\client\IClient;
use Zrx\Qdd\fdd\constants\OpenApiUrlConstants;

/**
 * 组织管理
 * 包含企业成员列表
 */
class OrgClient
{
    private $client;

    public function __construct(IClient $client)
    {
        $this->client = $client;
    }

    # 获取组织管理链接
    function getCorpOrganizationManageUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_ORGANIZATION_MANAGE_GET_URL);
    }

    # 创建部门
    function deptCreate(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_DEPT_CREATE);
    }

    # 查询部门列表
    function deptGetList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_DEPT_GET_LIST);
    }

    # 查询部门详情
    function deptGetDetail(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_DEPT_GET_DETAIL);
    }

    # 修改部门基本信息
    function deptModify(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_DEPT_MODIFY);
    }

    # 删除部门
    function deptDelete(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_DEPT_DELETE);
    }

    # 创建成员
    function memberCreate(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::CORP_MEMBER_CREATE);
    }

    # 获取成员激活链接
    function memberGetActiveUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::CORP_MEMBER_GET_ACTIVE_URL);
    }

    # 查询企业成员列表
    function getCorpMemberList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_MEMBER_GET_LIST);
    }

    # 查询成员详情
    function memberGetDetail(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_MEMBER_GET_DETAIL);
    }

    # 修改成员基本信息
    function memberModify(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::CORP_MEMBER_MODIFY);
    }

    # 设置成员所属部门
    function memberSetDept(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::CORP_MEMBER_SET_DEPT);
    }

    # 设置成员状态
    function memberSetStatus(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::CORP_MEMBER_SET_STATUS);
    }

    # 删除成员
    function memberDelete(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::CORP_MEMBER_DELETE);
    }
}
