<?php

namespace Zrx\Qdd\fdd\api;

use Zrx\Qdd\fdd\client\IClient;
use Zrx\Qdd\fdd\constants\OpenApiUrlConstants;

/**
 * 应用模板
 */
class AppTemplateClient
{
    private $client;

    public function __construct(IClient $client)
    {
        $this->client = $client;
    }

    # 查询应用文档模板列表
    function appDocTemplateGetList(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_DOC_TEMPLATE_GET_LIST);
    }

    # 获取应用文档模板详情
    function appDocTemplateGetDetail(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_DOC_TEMPLATE_GET_DETAIL);
    }

    # 查询应用签署任务模板列表
    function appSignTemplateGetList(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_SIGN_TEMPLATE_GET_LIST);
    }

    # 获取应用签署任务模板详情
    function appSignTemplateGetDetail(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_SIGN_TEMPLATE_GET_DETAIL);
    }

    # 获取应用模板新增链接
    function appTemplateCreateGetUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_TEMPLATE_CREATE_GET_URL);
    }

    # 获取应用模板编辑链接
    function appTemplateEditGetUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_TEMPLATE_EDIT_GET_URL);
    }

    # 获取应用模板预览链接
    function appTemplatePreviewGetUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_TEMPLATE_PREVIEW_GET_URL);
    }

    # 创建业务控件
    function appFieldCreate(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_FIELD_CREATE);
    }

    # 修改业务控件
    function appFieldModify(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_FIELD_MODIFY);
    }

    # 设置业务控件状态
    function appFieldSetStatus(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_FIELD_SET_STATUS);
    }

    # 查询业务控件列表
    function appFieldGetList(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::APP_FIELD_GET_LIST);
    }
}