<?php

namespace Zrx\Qdd\fdd\api;

use Zrx\Qdd\fdd\client\IClient;
use Zrx\Qdd\fdd\constants\OpenApiUrlConstants;

/**
 * 个人用户管理
 * 包含个人用户信息增删改查、认证、授权
 */
class UserClient
{
    private $client;

    public function __construct(IClient $client)
    {
        $this->client = $client;
    }

    # 获取个人用户授权链接
    function getUserAuthUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::USER_GET_AUTH_URL);
    }

    # 禁用个人用户
    function disable(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::USER_DISABLE);
    }

    # 恢复个人用户
    function enable(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::USER_ENABLE);
    }

    # 解绑个人用户账号
    function unbind(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::USER_UNBIND);
    }

    # 查询个人用户基本信息
    function getDetail(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::USER_GET_DETAIL);
    }

    # 获取个人用户身份信息
    function getIdentityInfo(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::USER_GET_IDENTITY_INFO);
    }
}