<?php

namespace Zrx\Qdd\fdd\api;

use Zrx\Qdd\fdd\client\IClient;
use Zrx\Qdd\fdd\constants\OpenApiUrlConstants;

/**
 * 企业用户账号管理
 * 包含企业用户信息增删改查、认证、授权
 */
class CorpClient
{

    private $client;

    public function __construct(IClient $client)
    {
        $this->client = $client;
    }

    # 获取企业用户授权链接
    function getCorpAuthUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::CORP_GET_AUTH_URL);
    }

    # 禁用企业用户
    function disable(string $accessToken,array $req)
    {
        return $this->client->request($accessToken,  json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_DISABLE);
    }

    # 恢复企业用户
    function enable(string $accessToken,array $req)
    {
        return $this->client->request($accessToken,  json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_ENABLE);
    }

    # 解除企业用户授权
    function unbind(string $accessToken,array $req)
    {
        return $this->client->request($accessToken,  json_encode($req, JSON_FORCE_OBJECT), OpenApiUrlConstants::CORP_UNBIND);
    }

    # 查询企业用户基本信息
    function getDetail(string $accessToken, array $req)
    {
        return $this->client->request($accessToken,  json_encode($req), OpenApiUrlConstants::CORP_GET_DETAIL);
    }

    # 获取企业用户身份信息
    function getIdentityInfo(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::CORP_GET_IDENTITY_INFO);
    }

    # 查询企业实名认证状态
    function getIdentifiedStatus(string $accessToken,array $req)
    {
        return $this->client->request($accessToken, json_encode($req), OpenApiUrlConstants::CORP_GET_IDENTIFIED_STATUS);
    }


}