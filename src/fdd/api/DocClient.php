<?php

namespace Zrx\Qdd\fdd\api;

use Zrx\Qdd\fdd\client\IClient;
use Zrx\Qdd\fdd\constants\OpenApiUrlConstants;

/**
 * 文件管理
 * 包含文件和附件上传
 */
class DocClient
{

    private $client;

    public function __construct(IClient $client)
    {
        $this->client = $client;
    }

    # 通过网络文件地址上传
    function fileUploadByUrl(string $accessToken,array $req)
    {
        return $this->client->request($accessToken,  json_encode($req), OpenApiUrlConstants::FILE_UPLOAD_BY_URL);
    }

    # 获取上传文件地址
    function fileGetUploadUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken,  json_encode($req), OpenApiUrlConstants::FILE_GET_UPLOAD_URL);
    }

    # PUT请求上传本地文件
    function fileUploadByLocal($url, $filePath)
    {
        return $this->client->request_file($url, $filePath);
    }

    # 文件处理
    function fileProcess(string $accessToken, array $req)
    {
        return $this->client->request($accessToken,  json_encode($req), OpenApiUrlConstants::FILE_PROCESS);
    }

    # 获取文件对比页面链接
    function getCompareUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken,  json_encode($req), OpenApiUrlConstants::OCR_EDIT_GET_COMPARE_URL);
    }

    # 获取历史文件对比页面链接
    function compareResultUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken,  json_encode($req), OpenApiUrlConstants::OCR_EDIT_COMPARE_RESULT_URL);
    }

    # 获取合同智审页面链接
    function getExamineUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken,  json_encode($req), OpenApiUrlConstants::OCR_EDIT_GET_EXAMINE_URL);
    }

    # 获取历史合同智审页面链接
    function examineResultUrl(string $accessToken, array $req)
    {
        return $this->client->request($accessToken,  json_encode($req), OpenApiUrlConstants::OCR_EDIT_EXAMINE_RESULT_URL);
    }


}
