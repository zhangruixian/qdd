<?php

namespace Zrx\Qdd\fdd;

use Exception;
use Zrx\Qdd\fdd\api\AppTemplateClient;
use Zrx\Qdd\fdd\api\CorpClient;
use Zrx\Qdd\fdd\api\DocClient;
use Zrx\Qdd\fdd\api\EUIClient;
use Zrx\Qdd\fdd\client\GetService;
use Zrx\Qdd\fdd\api\OrgClient;
use Zrx\Qdd\fdd\api\SealClient;
use Zrx\Qdd\fdd\api\ServiceClient;
use Zrx\Qdd\fdd\api\SignTaskClient;
use Zrx\Qdd\fdd\api\TemplateClient;
use Zrx\Qdd\fdd\api\UserClient;
use Zrx\Qdd\fdd\client\Client;
use Zrx\Qdd\fdd\constants\OpenApiConfigConstants;

/**
 * 法大大
 */
class Fdd
{
    //客户端
    public $client;
    //应用模板
    public $appTemplateClient;
    //企业用户账号管理
    public $corpClient;
    //文件管理
    public $docClient;
    //EUI页面链接管理
    public $EUIClient;
    //组织管理
    public $orgClient;
    //印章管理
    public $sealClient;
    //服务访问凭证
    public $serviceClient;
    //签署任务管理
    public $signTaskClient;
    //文档模板管理
    public $templateClient;
    //个人用户管理
    public $userClient;

    public function __construct()
    {
        //配置客户端基础信息
        $this->client = new Client(OpenApiConfigConstants::APP_ID, OpenApiConfigConstants::APP_SECRET, OpenApiConfigConstants::SERVICE_URL);
        $this->appTemplateClient = new AppTemplateClient($this->client);
        $this->corpClient = new CorpClient($this->client);
        $this->docClient = new DocClient($this->client);
        $this->EUIClient = new EUIClient($this->client);
        $this->orgClient = new OrgClient($this->client);
        $this->sealClient = new SealClient($this->client);
        $this->serviceClient = new ServiceClient($this->client);
        $this->signTaskClient = new SignTaskClient($this->client);
        $this->templateClient = new TemplateClient($this->client);
        $this->userClient = new UserClient($this->client);
    }

    public function fileUploadByUrl(){
        $fileUploadByUrlReq = new FileUploadByUrlReq();
        # 文件的用途类型：
        # doc：用于签署的文档，只有这种用途的文件才可以被加入签署任务的文档列表中，如果是文档类型，需要转换成PDF。
        # attach：用于签署文档的附件，只有这种用途的文件才可以被加入签署任务的附件列表中。
        $fileUploadByUrlReq->setFileType("doc");
        # 网络文件URL，法大大平台将从该地址拉取文档，长度最大为500个字符。
        # 注意：fileUrl需要进行编码。例：URLEncoder.encode("http://www.xxx.com/合同.doc", "UTF-8")。
        $fileUploadByUrlReq->setFileUrl(urlencode("https://anshu2022.oss-cn-shanghai.aliyuncs.com/2023-01/09/bb3f8fdf95c7ce0510f55d4781d0798e1b97bb11.PDF"));
//        dd($fileUploadByUrlReq);
        $url = "https://anshu2022.oss-cn-shanghai.aliyuncs.com/2023-01/09/bb3f8fdf95c7ce0510f55d4781d0798e1b97bb11.PDF";
        $arr = [
            'fileType'=>'doc',
            'fileUrl'=>urlencode($url),
        ];
        $response = $this->docClient->fileUploadByUrl($this->getAccessToken(),$arr);
        var_dump($response);die;
        //{"code":"100000","msg":"请求成功","data":{"fddFileUrl":"https://doc-test-os1.fadada.com/b2fbea50d3ba4db3a1df0b820a37120a"}}
    }

    /**
     * 获取Token
     * @return string
     * @throws Exception
     */
    public function getAccessToken(): string
    {
        $getService = new GetService();
        return $getService->getAccessToken();
    }

    public function model(){

    }

    /**
     * 签名
     * @param float $timestamp 时间戳
     * @param array $values 参数
     * @return false|string
     */
    public function getSign(float $timestamp,array $values){
        return $this->client->getSign($timestamp,$values);
    }

    /**
     * 获取时间戳
     * @return float
     */
    public function msectime(): float
    {
        list($msec, $sec) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
    }
}