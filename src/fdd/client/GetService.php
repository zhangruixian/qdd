<?php

namespace Zrx\Qdd\fdd\client;

use Exception;
use Zrx\Qdd\fdd\api\ServiceClient;
use Zrx\Qdd\fdd\constants\OpenApiConfigConstants;

class GetService
{
    public function __construct()
    {

    }

    /**
     * 获取token
     * @return string
     * @throws Exception
     */
    function getAccessToken() : string
    {
        /*****获取token******/
        $serviceClient = new ServiceClient(new Client(OpenApiConfigConstants::APP_ID, OpenApiConfigConstants::APP_SECRET, OpenApiConfigConstants::SERVICE_URL));

        $response = $serviceClient->getAccessToken();
        $res = json_decode($response);
        if ($res->code != 100000){
            throw new Exception($res->msg);
        }
//        {"code":"100000","msg":"请求成功","data":{"accessToken":"d9176b75b51142c9976e299314bed612","expiresIn":"7200"}}
        return $res->data->accessToken;
    }

    function getAppAccessTicket(){
        /*****获取token******/
        $serviceClient = new ServiceClient(new Client(OpenApiConfigConstants::APP_ID, OpenApiConfigConstants::APP_SECRET, OpenApiConfigConstants::SERVICE_URL));

        $response = $serviceClient->getAppAccessTicket();
        print_r($response."\n");
        $res = json_decode($response);
        return $res->data->accessToken;
    }

    function getUserAccessTicket(){
        /*****获取token******/
        $serviceClient = new ServiceClient(new Client(OpenApiConfigConstants::APP_ID, OpenApiConfigConstants::APP_SECRET, OpenApiConfigConstants::SERVICE_URL));

        $response = $serviceClient->getUserAccessTicket();
        print_r($response."\n");
        $res = json_decode($response);
        return $res->data->accessToken;
    }
}
